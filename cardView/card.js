      let btn_next = $('#card-custom_btn_next');
        let btn_prev = $('#card-custom_btn_prev');
        let slide    = $('#card-custom');
        let slide_mrgin_t_move =25 ; // how many pixels margin to move
        let current_margin = 0; // initially is 0
        let max_margin_left_slide = -100; // use dinamically! 
        let max_margin_right_slide = 0; // use dinamically!
        
        $( document ).ready( function () {
            //slide.css('width', '700%'); // like this you can modify width  

            btn_next.on('click' , function () {
                card_next();
            });

            btn_prev.on('click', function () {
               card_prev();
            });
        
        });
             
        function card_next() {
           if ( current_margin > max_margin_left_slide ) {
                current_margin = current_margin - slide_mrgin_t_move;
                slide.animate({marginLeft: '' + current_margin + '%' });
           }

        }
        
        function card_prev() {
            if ( current_margin < max_margin_right_slide ) {
                current_margin = current_margin + slide_mrgin_t_move;
                slide.animate({marginLeft: '' + current_margin + '%' });               
            }

        }